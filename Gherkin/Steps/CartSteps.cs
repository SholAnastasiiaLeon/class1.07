﻿using System;
using TechTalk.SpecFlow;

namespace Gherkin.Steps
{
    [Binding]
    public class CartSteps
    {
        [Given(@"Allo website is open")]
        public void GivenAlloWebsiteIsOpen()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"User is not logged in")]
        public void GivenUserIsNotLoggedIn()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User click on cart")]
        public void WhenUserClickOnCart()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"Cart is empty")]
        public void ThenCartIsEmpty()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
