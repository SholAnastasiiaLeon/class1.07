﻿Feature: Information
    As a user
    I want to have possibility change language 
    So that I can read information on site in selected language

Scenario: Change language
    Given Allo website is open
    Given User is not logged in
    When I enter dropdown for cities and enter in Dnipro city button
    Then I can see products in Dniro city

Feature: Subscription
     As a user 
     I want to subscribe on newsletter
     So that I can get news about discount for products
Scenario: Subscription on newsletter
    Given Allo website is open
    Given User is not logged in
    When I enter email in email field
    When I enter subscribe button
    Then I can see messages that subscription successfuly

Feature: Information
     As a user 
     I want to read information about delivery
     So that I can choice comfortable delivery method
Scenario: Check information about
    Given Allo website is open
    Given User is not logged in
    When User clicks on delivery button
    Then User can read information about delivery methods


Feature: Information
     As a dealer
     I want to have map with shops
     So that user can find shop and see information about shops work hour
Scenario: Choose store in header
    Given Allo website is open
    Given User is not logged in
    When User clicks on store button
    Then  User can find map and information about shops work hour



Feature: Information
    As a user
    I want to change the city on the site
    So that I can see current products in the selected city
Scenario: Choose city in header
    Given Allo website is open
    Given User is not logged in
    When User click on city button
    Then  User can see products in current city