﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;

namespace Gherkin.POM
{
    public class CartPOM
    {
        private IWebDriver _driver;

        public By labelCart = By.XPath("/html/body/div[3]/div/div/div[2]/span");
        public By YourCartIsPusto= By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[1]");
        public By returnToBuying = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[2]");
        public By ssylka = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[2]/a");
        public By closeKorziny = By.XPath("/html/body/div[3]/div/div/div[1]/svg/use//svg/path");
    }
}
